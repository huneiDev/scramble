﻿using UnityEngine;
using System.Collections;

public class ScreenManager : MonoBehaviour {


    public static float GetLeftEdgePoint(float radius) {

        Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(Mathf.Abs(Screen.width), Screen.height / 2, 0));
        float final = -point.x + radius;
        return final;
    }
    public static float GetDownPoint(float radius) {
        Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, (Mathf.Abs(Screen.height)), 0));
        float final = -point.y - radius;
        return final;
    }


    public static float GetUpPoint() {
        Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height, 0));
        float final = point.y - 0.5f;
        return final;

    }


}
