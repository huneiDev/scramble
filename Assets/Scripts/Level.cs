﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Level : MonoBehaviour {

    [Header("Level values:")]
    [SerializeField]
    private bool _hasCentre;
    [SerializeField]
    private int _ammountOfCircles;
    [SerializeField]
    private float _ammountOfSpacing;

    private int maxId = 0;

    public const float sphereRadius = 0.5f;

    private List<GameObject> allCircles = new List<GameObject>();
    public List<GameObject> currentCircles = new List<GameObject>();

    public List<Color> CircleColorPallete;

    [SerializeField]
    private GameObject defaultCircle;

    public GameObject RightCircle;


    private static Level _instance;

    public static Level Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<Level>();
                if (_instance == null) {
                    Debug.Log("Error searching for singleton: Level");
                }
            }
            return _instance;
        }
    }

    public void ResetValues() {
        
        RightCircle = null;
        currentCircles.Clear();
        maxId = 0;

    }

    public IEnumerator OnLevelFailed() {
        yield return null;
        foreach(GameObject c in currentCircles) {
            c.transform.DOMoveY(ScreenManager.GetUpPoint() + 1f,1.5f, false).SetEase(Ease.InSine);

        }
        yield return new WaitForSeconds(1.6f);
        foreach (GameObject c in currentCircles) {
            c.SetActive(false);
            Destroy(c.GetComponent<Circle>());
            c.AddComponent<Circle>();
        }
        UIManager.Instance.SetCanvas(UIManager.CanvasType.Failed);

    }


    public IEnumerator OnCompleteLevel() {

        foreach (GameObject c in currentCircles) {
            c.transform.DOMoveY(ScreenManager.GetDownPoint(0.5f), TweenManager.Instance.CircleOnCompleteTime, false).SetEase(TweenManager.Instance.CircleOnCompleteEase);
        }
        yield return new WaitForSeconds(TweenManager.Instance.CircleOnCompleteTime + 0.1f);
        foreach (GameObject c in currentCircles) {
            c.SetActive(false);
            Destroy(c.GetComponent<Circle>());
            c.AddComponent<Circle>();
            
        }
        ResetValues();
        Game.Instance.CurrentLevel++;
        Initialize();



    }

    private void ApplyRandomColors() {
        List<Color> usedColors = new List<Color>();
        for (int i = 0; i < currentCircles.Count; i++) {
            int ranId = Random.Range(0, CircleColorPallete.Count);
            Color ranColor = CircleColorPallete[ranId];
            while (usedColors.Contains(ranColor)) {
                ranId = Random.Range(0, CircleColorPallete.Count);
                ranColor = CircleColorPallete[ranId];
            }

            usedColors.Add(ranColor);
            currentCircles[i].GetComponent<MeshRenderer>().material.color = ranColor;
        }


    }


    private void PoolDefaultCircles() {
        for (int i = 0; i < Game.Instance.DifficultyCurve[1].value; i++) {
            GameObject g = Instantiate(defaultCircle);
            g.SetActive(false);
            allCircles.Add(g);


        }
    }

    private Circle GetRandomCircle() {
        int ranID = Random.Range(0, maxId);
        while (currentCircles[ranID].GetComponent<Circle>().IsScrambling) {
            ranID = Random.Range(0, maxId);
        }

        currentCircles[ranID].GetComponent<Circle>().IsScrambling = true;
        return currentCircles[ranID].GetComponent<Circle>();

    }

    private IEnumerator SetRandomRightCircle() {
        int ranRightCircle = Random.Range(0, maxId);
        RightCircle = currentCircles[ranRightCircle];
        RightCircle.transform.DOScale(1.3f, TweenManager.Instance.MainCirclePopTime).SetEase(TweenManager.Instance.MainCirclePopTimeEase);
        yield return new WaitForSeconds(TweenManager.Instance.MainCirclePopTime + 0.3f);
        RightCircle.transform.DOScale(1, TweenManager.Instance.MainCirclePopTime - 0.2f).SetEase(Ease.OutBack);


    }

    private IEnumerator BeginScrambling() {

        yield return new WaitForSeconds(TweenManager.Instance.CircleMoveInTime + 0.3f);
        StartCoroutine(SetRandomRightCircle());
        yield return new WaitForSeconds(TweenManager.Instance.MainCirclePopTime + 0.6f);
        for (int i = 0; i < Game.Instance.AmmountOfScrambles; i++) {
            GetRandomCircle().Scramble(GetRandomCircle());
            yield return new WaitForSeconds(Game.Instance.ScramblingSpeed + 0.1f);
        }
        yield return null;
    }

    private void ScaleCircles() {


    }

    private void Start() {
        Initialize();
    }
    private void Awake() {
        PoolDefaultCircles();
    }

    private void PositionCircles() {

        float currentX = ScreenManager.GetLeftEdgePoint(sphereRadius);
        for (int i = 0; i < _ammountOfCircles; i++) {
            currentCircles[i].transform.position = new Vector3(currentX + _ammountOfSpacing, ScreenManager.GetDownPoint(sphereRadius), 0);
            currentX += sphereRadius * 2;
        }

    }

    private void ActivateCircles() {
        Debug.Log(_ammountOfCircles);
        Debug.Log(allCircles.Count);
        Debug.Log(currentCircles.Count);
        for (int i = 0; i < allCircles.Count; i++) {
            if (allCircles[i].gameObject.activeSelf)
                continue;
            if(i >=_ammountOfCircles) 
                continue;
            
            allCircles[i].SetActive(true);
            allCircles[i].GetComponent<Circle>().Init(maxId);
            currentCircles.Add(allCircles[i]);
            maxId++;
        }
    }

    public void Initialize() {
        _ammountOfCircles = Game.Instance.AmmountOfCirclesToSpawn;
        Camera.main.orthographicSize = _ammountOfCircles;
        if ((float)Game.Instance.AmmountOfCirclesToSpawn / 2 != Mathf.Floor(Game.Instance.AmmountOfCirclesToSpawn / 2))
            _hasCentre = true;


        ActivateCircles();
        PositionCircles();
        ApplyRandomColors();
        StartCoroutine(BeginScrambling());
    }


}
