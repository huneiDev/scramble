﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {


    private static UIManager _instance;

    public static UIManager Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<UIManager>();
                if(_instance == null) {
                    Debug.Log("Instance is null: UIManager");
                }
            }
            return _instance;
        }
    }

    public enum CanvasType {
       Playing,
       Failed
    }
    
    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private GameObject failedCanvas;

    [SerializeField]
    private GameObject playCanvas;
    


       
    private void UpdateScore(string output) {
        scoreText.text = output;
        scoreText.GetComponent<RectTransform>().DOScale(1, TweenManager.Instance.ScoreInTime).SetEase(TweenManager.Instance.ScoreInEase);
    }

    public void UpdateText(string type,string output) {
        switch (type) {
            case "score":
                scoreText.GetComponent<RectTransform>().DOScale(0, TweenManager.Instance.ScoreOutTime)
                    .SetEase(TweenManager.Instance.ScoreOutEase)
                    .OnComplete(()=>UpdateScore(output));

                break;
        }
    }

    public void Retry() {
        SceneManager.LoadScene("Main");
    }

    public void SetCanvas(CanvasType Canvas) {
        switch (Canvas) {
            case CanvasType.Failed:
                playCanvas.SetActive(false);
                failedCanvas.SetActive(true);

                break;
        }
    }




}
