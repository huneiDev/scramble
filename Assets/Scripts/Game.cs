﻿
using UnityEngine;
using System.Collections;


public class Game : MonoBehaviour {

    private static Game _instance;

    public static Game Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<Game>();
                if(_instance == null) {
                    Debug.Log("error searching for singleton: Game");
                }
            }
            return _instance;
        }
    }


    public AnimationCurve DifficultyCurve;

    public AnimationCurve ScramblesCurve;

    public AnimationCurve ScramblingSpeedCurve;

    private int _currentLevel = 0;

    private void Start() {
        CurrentLevel = 1;
    }


    
    public int CurrentLevel {
        get {
            return _currentLevel;
        }
        set {
            _currentLevel = value;
            UIManager.Instance.UpdateText("score", _currentLevel.ToString());
        }
    }

    public float ScramblingSpeed {
        get {
            return ScramblingSpeedCurve.Evaluate(CurrentLevel);
        }
    }
    
    public int AmmountOfScrambles {
        get {
            return Mathf.CeilToInt(ScramblesCurve.Evaluate(CurrentLevel));
        }
    }

    public int AmmountOfCirclesToSpawn {
        get {
            return Mathf.CeilToInt(DifficultyCurve.Evaluate(CurrentLevel));
        }
    }



}