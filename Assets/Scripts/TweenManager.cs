﻿
using UnityEngine;
using System.Collections;

public class TweenManager : MonoBehaviour {

    private static TweenManager _instance;

    public static TweenManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<TweenManager>();
                if (_instance == null) {
                    Debug.Log("Error searching for instance for tweenData");

                }

            }
            return _instance;
        }
    }

    private void Awake() {
        _instance = this;
    }

    [Header("Circle Tweens")]
    [Range(0, 2f)]
    public float CircleMoveInTime;
    public DG.Tweening.Ease CircleMoveInEase;
    
    public DG.Tweening.Ease CircleScramblingEase;
    public DG.Tweening.Ease CircleColorScramblingEase;
    [Range(0, 2f)]
    public float MainCirclePopTime;
    public DG.Tweening.Ease MainCirclePopTimeEase;
    [Range(0, 2f)]
    public float CircleOnFailTime;
    public DG.Tweening.Ease CircleOnFailEase;
    [Range(0, 2f)]
    public float CircleOnCompleteTime;
    public DG.Tweening.Ease CircleOnCompleteEase;

    [Header("UI Tweens")]
    [Range(0, 2f)]
    public float ScoreOutTime;
    public DG.Tweening.Ease ScoreOutEase;
    [Range(0, 2f)]
    public float ScoreInTime;
    public DG.Tweening.Ease ScoreInEase;


}
