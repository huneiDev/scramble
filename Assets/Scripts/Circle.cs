﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Circle : MonoBehaviour {

    public int SortingIndex;
    public Color CurrentColor;
    public bool IsScrambling;
    public Vector3 originPosition;

    private float bounds {
        get {
            return ScreenManager.GetUpPoint() / 2;
        }
    }


    [SerializeField]
    private Ease ScrambleMoveEase;

    [SerializeField]
    private Ease ScrambleColorEase;

    public float ScrambleTime;

    public void Init(int id) {
        SortingIndex = id;
        CurrentColor = GetComponent<MeshRenderer>().material.color;
    }


    public void SwapValues(Circle c) {
        var cSorting = c.SortingIndex;
        var cColor = c.CurrentColor;
        var cPos = c.originPosition;

        c.SortingIndex = SortingIndex;
        c.CurrentColor = CurrentColor;
        c.originPosition = originPosition;
        c.IsScrambling = false;

        SortingIndex = cSorting;
        CurrentColor = cColor;
        originPosition = cPos;
        IsScrambling = false;
    }


    public void Scramble(Circle c) {

        Circle temp = c;
        Circle temp2 = this.GetComponent<Circle>();
        transform.DOMove(temp.transform.position, Game.Instance.ScramblingSpeed, false).OnComplete(() => SwapValues(temp)).SetEase(TweenManager.Instance.CircleScramblingEase);
        c.transform.DOMove(temp2.transform.position, Game.Instance.ScramblingSpeed, false).SetEase(TweenManager.Instance.CircleScramblingEase);
        GetComponent<MeshRenderer>().material.DOColor(temp.GetComponent<MeshRenderer>().material.color, Game.Instance.ScramblingSpeed).SetEase(TweenManager.Instance.CircleColorScramblingEase);
        c.GetComponent<MeshRenderer>().material.DOColor(temp2.GetComponent<MeshRenderer>().material.color, Game.Instance.ScramblingSpeed).SetEase(TweenManager.Instance.CircleColorScramblingEase);


    }

    private void OnMoveUpComplete() {
        CurrentColor = GetComponent<MeshRenderer>().material.color;
        originPosition = this.transform.position;
    }

    private void OnDisable() {
        //Destroy(GetComponent<Circle>());

    }

    private void CheckBounds() {
        if (transform.position.y >= bounds || transform.position.y <= -bounds) {
            if(this.gameObject == Level.Instance.RightCircle) {
                StartCoroutine(Level.Instance.OnCompleteLevel());
            } else {
                StartCoroutine(Level.Instance.OnLevelFailed());
            }


        } else {
            if (transform.position != originPosition) {
                transform.DOMove(originPosition, 0.5f, false).SetEase(Ease.InOutCubic);
            }
        }
    }

    private void OnMouseUp() {
        CheckBounds();
    }


    private void OnMouseDrag() {
        OnDrag(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)));
    }

    private void OnDrag(Vector2 dragPos) {
        this.transform.DOMove(dragPos, 0.3f, false);


    }

    private void Update() {

    }
    private void MoveUp() {
        transform.DOMoveY(0, TweenManager.Instance.CircleMoveInTime, false).SetEase(TweenManager.Instance.CircleMoveInEase).SetDelay(0.5f).OnComplete(() => OnMoveUpComplete());
    }

    private void OnEnable() {
        MoveUp();

    }


}